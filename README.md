# Cambridge D10 Profile

## Status

This repository is under continued development and build currently.

The contents of this repository have not been yet released for a formal v1.0 and we may introduce breaking changes into the repository and its content without warning at any point. This is to be used at your own risk.

If you have any questions regarding this, please contact webmaster@admin.cam.ac.uk.

## Local development with Gitflow

Local development of this repository uses gitflow.

Setup and usage can be found here: https://danielkummer.github.io/git-flow-cheatsheet/

Note, all defaults should be used during setup, so that (for example), `git flow feature start AM-123` results in a local branch named `git flow feature start feature/AM-123`

